package net.cuebria.language;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import net.cuberia.language.LanguageManager;
import net.cuberia.language.languages.MemoryLanguage;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2017 Johannes Pollitt
 * All rights reserved
 */

class LanguageManagerTest {
	
	@Test
	void test() {

		LanguageManager lm = new LanguageManager();
		
		MemoryLanguage de = new MemoryLanguage("de", "Deutsch");
		de.addTranslation("hello", "Hallo");
		de.addTranslation("world", "Welt");
		
		MemoryLanguage en = new MemoryLanguage("en", "English");
		en.addTranslation("hello", "Hello");
		en.addTranslation("world", "World");
		en.addTranslation("name", "Johannes");
		
		lm.getLanguagePool().addLanguage(en);
		lm.getLanguagePool().addLanguage(de);
		
		lm.setDefaultLanguage(en);
		
		assertEquals("Hello", lm.getTranslation("hello"));
		assertEquals("World", lm.getTranslation("world"));
		assertEquals("Johannes", lm.getTranslation("name"));
		
		lm.setCurrentLanguage(de);
		
		assertEquals("Hallo", lm.getTranslation("hello"));
		assertEquals("Welt", lm.getTranslation("world"));
		assertEquals("Johannes", lm.getTranslation("name"));
		assertEquals("Pollitt", lm.getTranslation("name", "Pollitt"));
		
	}

}
