package net.cuebria.language.languages;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import net.cuberia.language.languages.MemoryLanguage;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2017 Johannes Pollitt
 * All rights reserved
 */

class MemoryLanguageTest {

	@Test
	void test() {
		MemoryLanguage lang = new MemoryLanguage("en", "English");
		assertEquals("en", lang.getID());
		assertEquals("English", lang.getName());
		assertEquals(0, lang.getKeys().size());
		
		lang.addTranslation("hello", "Hello");
		
		assertEquals("Hello", lang.getTranslation("hello"));
		lang.addTranslation("hello", "Hello World");
		assertEquals("Hello World", lang.getTranslation("hello"));
		
	}

}
