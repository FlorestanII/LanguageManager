package net.cuberia.language;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import net.cuberia.language.exception.LanguagePoolException;
import net.cuberia.language.languages.ILanguage;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2018 Johannes Pollitt
 * All rights reserved
 */

public class LanguagePool {
	
	private final Map<String, ILanguage> languages;
	
	/**
	 * Creates a new empty {@link LanguagePool}
	 * 
	 */
	public LanguagePool() {
		this.languages = new HashMap<String, ILanguage>();
	}
	
	/**
	 * Clears this {@link LanguagePool}.
	 * 
	 */
	public void clear() {
		this.languages.clear();
	}
	
	/**
	 * Adds the supplied language to this pool
	 * You can not replace an existing language with this method! 
	 * To do this you have to first remove the old language.
	 * 
	 * @param lang the new language
	 * @throws LanguagePoolException if there is already a language added with the same id
	 */
	public void addLanguage(ILanguage lang) throws LanguagePoolException {
		
		if (this.languages.containsKey(lang.getID())) {
			throw new LanguagePoolException("There is already a language with the same id: " + lang.getID());
		}
		
		this.languages.put(lang.getID(), lang);
	}
	
	/**
	 * Adds the supplied language to this pool
	 * You can not replace an existing language with this method! 
	 * To do this you have to first remove the old language.
	 * 
	 * @param lang the new language
	 * @throws LanguagePoolException if there is already a language added with the same id
	 */
	public void addLanguages(Collection<ILanguage> languages) throws LanguagePoolException {
		
		if (languages == null) {
			return;
		}
		
		languages.forEach((lang) -> {
			if (this.languages.containsKey(lang.getID())) {
				throw new LanguagePoolException("There is already a language with the same id: " + lang.getID());
			}
			
			this.languages.put(lang.getID(), lang);
		});
		
	}
	
	/**
	 * Returns a collection with all languages added to this pool.
	 * 
	 * @return collection with all languages
	 */
	public Collection<ILanguage> getLanguages() {
		return languages.values();
	}
	
	/**
	 * Removes the language from this pool with the supplied id.
	 * If there is no language with the supplied id an empty {@link Optional} will be returned. 
	 * 
	 * @param id the id of the language to remove
	 * @return the removed language
	 */
	public Optional<ILanguage> removeLanguage(String id) {
		return Optional.ofNullable(this.languages.remove(id));
	}
	
	/**
	 * Removes the languages from this pool which is equals to the supplied language.
	 * If there is no such language an empty {@link Optional} will be returned. 
	 * 
	 * @param language the language to be removed
	 * @return the removed language
	 */
	public Optional<ILanguage> removeLanguage(ILanguage language) {
		
		if (language == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(this.languages.remove(language.getID()));
	}
	
	/**
	 * Returns an {@link Optional} with the {@link ILanguage} object which has the same id as the supplied id.
	 * Returns an empty {@link Optional} if there is no language with this id.
	 * 
	 * @param id the id of the language
	 * @return {@link ILanguage} object
	 */
	public Optional<ILanguage> getLanguage(String id) {
		
		if (id == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(this.languages.get(id));
	}
	
	/**
	 * Checks if there is a language in this pool with the same id as the supplied id.
	 * 
	 * @param id the id to check
	 * @return if this pool contains a language with the supplied id
	 */
	public boolean containsLanguage(String id) {
		if (id == null) {
			return false;
		}
		
		return this.languages.containsKey(id);
	}
	
	/**
	 * Checks if there is a language which is equals to the supplied language.
	 * 
	 * @param language the language to check
	 * @return if this pool contains the supplied language 
	 */
	public boolean containsLanguage(ILanguage language) {
		if (language == null) {
			return false;
		}
		
		return containsLanguage(language.getID());
	}

	/**
	 * Returns the translation of the supplied key in the supplied language.
	 * 
	 * @param langID the id of the language
	 * @param key the translation key
	 */
	public String getTranslation(String langID, String key) {
		
		if (key == null) {
			return LanguageManager.INVALID_KEY;
		}
		
		if (containsLanguage(langID)) {
			return getLanguage(langID).get().getTranslation(key);
		} else {
			return LanguageManager.INVALID_LANGUAGE;
		}
		
	}
	
	/**
	 * Returns the translation of the supplied key in the supplied language.
	 * If the supplied language does not exists or the key does not exists the defaultValue will be returned.
	 * 
	 * @param langID the id of the language
	 * @param key the translation key
	 * @param defaultValue the default value
	 */
	public String getTranslation(String langID, String key, String defaultValue) {
		
		if (key == null) {
			return defaultValue;
		}
		
		if (containsLanguage(langID)) {
			return getLanguage(langID).get().getTranslation(key, defaultValue);
		} else {
			return defaultValue;
		}
		
	}
	
}
