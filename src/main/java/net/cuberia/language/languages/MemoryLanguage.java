package net.cuberia.language.languages;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.cuberia.language.LanguageManager;
import net.cuberia.language.exception.LanguageException;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2018 Johannes Pollitt
 * All rights reserved
 */

public class MemoryLanguage implements ILanguage{
		
	private final String id;
	private final String name;
	
	private final Map<String, String> values;
	
	/**
	 * Creates a new {@link MemoryLanguage} with the supplied <code>id</code> and <code>name</code>.
	 * 
	 * @param id the id of this language
	 * @param name the name of this language
	 * @throws IllegalArgumentException if id or name is equals null
	 */
	public MemoryLanguage(String id, String name) {
		
		if (id == null) {
			throw new LanguageException("The id of this language must not be null!");
		}
		
		if (name == null) {
			throw new LanguageException("The name of this language must not be null!");
		}
		
		this.id = id;
		this.name = name;
		
		this.values = new HashMap<String, String>();
	}
	
	/**
	 * Creates a new {@link MemoryLanguage} with the supplied <code>id</code> and <code>name</code> and the supplied start <code>values</code>.
	 * 
	 * @param id the id of this language
	 * @param name the name of this language
	 * @param values start values for this language
	 * @throws IllegalArgumentException if id or name is equals null
	 */
	public MemoryLanguage(String id, String name, Map<String, String> values) {
	
		if (id == null) {
			throw new LanguageException("The id of this language must not be null!");
		}
		
		if (name == null) {
			throw new LanguageException("The name of this language must not be null!");
		}
		
		this.id = id;
		this.name = name;
		
		this.values = values;
	}
	
	/**
	 * Creates a new {@link MemoryLanguage} as a copy of the supplied <code>lang</code>.
	 * 
	 * @param lang the source language
	 * @throws IllegalArgumentException if <code>lang</code> is equals null
	 */
	public MemoryLanguage(ILanguage lang) {
		
		if (lang == null) {
			throw new LanguageException("The source language must not be null!");
		}
		
		this.id = lang.getID();
		this.name = lang.getName();
		
		this.values = new HashMap<String, String>();
		
		lang.getKeys().forEach((key) -> {
			this.addTranslation(key, lang.getTranslation(key));
		});
	}
	
	@Override
	public String getID() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getTranslation(String key, String defaultValue) {
		
		if (key == null) {
			return LanguageManager.INVALID_KEY;
		}
		
		String value = values.get(key);
		
		if (value == null) {
			value = defaultValue;
		}
		
		return value;
	}

	@Override
	public String getTranslation(String key) {

		if (key == null) {
			return LanguageManager.INVALID_KEY;
		}
		
		String value = values.get(key);
		
		if (value == null) {
			value = LanguageManager.INVALID_KEY;
		}
		
		return value;
	}

	@Override
	public Set<String> getKeys() {
		return values.keySet();
	}

	/**
	 * Clears all keys and translations from the memory of this {@link MemoryLanguage}.
	 * 
	 */
	public void clear() {
		values.clear();
	}
	
	/**
	 * Adds a new translation to this language.
	 * If the key already exists the value will be replaced.
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public void addTranslation(String key, String value) {
		this.values.put(key, value);
	}
	
	/**
	 * Adds new translations to this language.
	 * If a key already exists the value will be replaced.
	 * 
	 * @param values map with all new keys and values
	 */
	public void addTranslations(Map<String, String> values) {
		this.values.putAll(values);
	}
	
	@Override
	public String toString() {
		return "{id=\"" + id + "\", name=\"" + name + "\"}";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof ILanguage) {
				ILanguage lang = (ILanguage) obj;
				if (lang.getID().equals(this.getID())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
}
