package net.cuberia.language.languages;

import java.util.Set;

import net.cuberia.language.exception.LanguageException;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2018 Johannes Pollitt
 * All rights reserved
 */

public interface ILanguage {
	
	/**
	 * Returns the unique identifier of this language.
	 * 
	 * @return the unique identifier
	 */
	public String getID();
	
	/**
	 * gets the full name of this language.
	 * 
	 * @return the name
	 */
	public String getName();
	
	/**
	 * Returns the translation of the supplied <code>key</code>.
	 * If there is no translation for the <code>key</code>, the <code>defaultValue</code> will be returned.
	 * 
	 * @return key the translation key
	 * @return defaultValue th default value
	 * @return the translation
	 * @throws LanguageException if the key is equals null
	 */
	public String getTranslation(String key, String defaultValue);
	

	/**
	 * Returns the translation of the supplied <code>key</code>.
	 * If there is no translation for the <code>key</code>, the <code>key</code> will be returned.
	 *  
	 * @param key the translation key
	 * @return the translation
	 * @throws LanguageException if the key is equals null
	 */
	public String getTranslation(String key);
	
	/**
	 * Returns all keys available in this language.
	 * 
	 * @return all keys available in this language
	 */
	public Set<String> getKeys();
}
