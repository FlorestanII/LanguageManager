package net.cuberia.language;

import java.util.Optional;

import net.cuberia.language.exception.UnknownLanguageException;
import net.cuberia.language.languages.ILanguage;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2018 Johannes Pollitt
 * All rights reserved
 */

public class LanguageManager {

	public static final String INVALID_LANGUAGE = "{ERR_INVALID_LANG}";
	public static final String INVALID_KEY = "{ERR_INVALID_KEY}";

	private LanguagePool langPool;
	
	private String defaultLanguageID;
	private String currentLanguageID;
	
	/**
	 * Creates a new {@link LanguageManager}
	 * 
	 */
	public LanguageManager() {
		this.langPool = new LanguagePool();
	}
	
	/**
	 * Creates a new {@link LanguageManager} with the supplied  {@link LanguagePool}.
	 * 
	 * @param languagePool the source languagePool
	 */
	public LanguageManager(LanguagePool languagePool) {
		this.langPool = languagePool;
	}
	
	/**
	 * Returns the language pool of this {@link LanguageManager}.
	 * 
	 * @return the language pool
	 */
	public LanguagePool getLanguagePool() {
		return langPool;
	}
	
	/**
	 * Sets the current language of this manager to the supplied id.
	 * 
	 * @param id the id of the new current language
	 * @throws UnknownLanguageException if there is no language with the supplied id
	 */
	public void setCurrentLanguage(String id) {
		if (!langPool.containsLanguage(id)) {
			throw new UnknownLanguageException("Tried to set an unknown language as current language: " + id);
		}
		this.currentLanguageID = id;
	}
	
	/**
	 * Sets the current language of this manager to the id of the supplied language.
	 * If the supplied language is not added yet it will be added.
	 * 
	 * @param language the new current language
	 * @throws UnknownLanguageException if the supplied language is null
	 */
	public void setCurrentLanguage(ILanguage language) {
		
		if (language == null) {
			throw new UnknownLanguageException("The supplied language must not be null!");
		}
		
		if (!langPool.containsLanguage(language)) {
			langPool.addLanguage(language);
		}
		this.currentLanguageID = language.getID();
	}
	
	/**
	 * Returns the current language.
	 * If there is no valid current language the default language will be returned.
	 * 
	 * @return the current language
	 */
	public Optional<ILanguage> getCurrentLanguage() {
		
		if (langPool.containsLanguage(currentLanguageID) == false) {
			return langPool.getLanguage(defaultLanguageID);
		}
		
		return langPool.getLanguage(currentLanguageID);
	}
	
	/**
	 * Returns the id of the current language.
	 * If there is no valid current language the id of the default language will be returned.
	 * 
	 * @return the id of the current language
	 */
	public Optional<String> getCurrentLanguageID() {
		
		if (langPool.containsLanguage(currentLanguageID) == false) {
			return Optional.ofNullable(defaultLanguageID);
		}
		
		return Optional.of(currentLanguageID);
	}
	
	/**
	 * Resets the current language to the default language.
	 * 
	 */
	public void resetCurrentLanguage() {
		this.currentLanguageID = defaultLanguageID;
	}
	
	/**
	 * Sets the default language to the supplied id.
	 * 
	 * @param id the new default language id
	 * @throws UnknownLanguageException if there is no language with the supplied id
	 */
	public void setDefaultLanguage(String id) {
		if (!langPool.containsLanguage(id)) {
			throw new UnknownLanguageException("Tried to set an unknown language as default language: " + id);
		}
		this.defaultLanguageID = id;
	}
	
	/**
	 * Sets the default language to the id of the supplied language.
	 * If the supplied language is not added yet it will be added.
	 * 
	 * @param language the new default language
	 * @throws UnknownLanguageException if the supplied language is null
	 */
	public void setDefaultLanguage(ILanguage language) {
		
		if (language == null) {
			throw new UnknownLanguageException("The supplied language must not be null!");
		}
		
		if (!langPool.containsLanguage(language)) {
			langPool.addLanguage(language);
		}
		this.defaultLanguageID = language.getID();
	}
	
	/**
	 * Returns the default language of this manager.
	 * 
	 * @return the default language
	 */
	public Optional<ILanguage> getDefaultLanguage() {
		return langPool.getLanguage(defaultLanguageID);
	}
	
	/**
	 * Returns the id of the default language.
	 * 
	 * @return the id of the default language
	 */
	public Optional<String> getDefaultLanguageID() {
		return Optional.ofNullable(defaultLanguageID);
	}
	
	/**
	 * Clears this entire manager.
	 * Clears the language pool and sets current and default language to null.
	 * 
	 */
	public void clear() {
		this.langPool.clear();
		this.currentLanguageID = null;
		this.defaultLanguageID = null;
	}
	
	/**
	 * Returns the translation of the supplied key.
	 * The current language will be used. If it is not valid the default language will be used.
	 * 
	 * @param key the key
	 * @return the translation
	 */
	public String getTranslation(String key) {
		if (getCurrentLanguage().isPresent()) {
			String value = langPool.getTranslation(getCurrentLanguageID().get(), key);
			
			if (value == null || value.equals(INVALID_KEY) || value.equals(INVALID_LANGUAGE)) {
				if (getDefaultLanguage().isPresent()) {
					value = langPool.getTranslation(getDefaultLanguageID().get(), key);
				} else {
					value = INVALID_LANGUAGE;
				}
			}
			
			return value;
		}
		if (getDefaultLanguage().isPresent()) {
			return langPool.getTranslation(getDefaultLanguageID().get(), key);
		} else {
			return INVALID_LANGUAGE;
		}
	}
	
	/**
	 * Returns the translation of the supplied key.
	 * The current language will be used. If it is not valid the default language will be used.
	 * If the language or the key is invalid the <code>defaultValue</code> will be returned.
	 * 
	 * @param key the key
	 * @param defaultValue the default value
	 * @return the translation
	 */
	public String getTranslation(String key, String defaultValue) {
		if (getCurrentLanguage().isPresent()) {
			String value = langPool.getTranslation(getCurrentLanguageID().get(), key);
			
			if (value == null || value.equals(INVALID_KEY) || value.equals(INVALID_LANGUAGE)) {
				
				value = defaultValue;
			
			}
			
			return value;
		}
		return defaultValue;
	}
	
}
