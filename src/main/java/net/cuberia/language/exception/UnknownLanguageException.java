package net.cuberia.language.exception;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2017 Johannes Pollitt
 * All rights reserved
 */

public class UnknownLanguageException extends LanguageException {

	
	private static final long serialVersionUID = -1760070706267802888L;

	/**
	 * Creates a new {@link UnknownLanguageException}
	 * 
	 * @param msg the exception message
	 */
	public UnknownLanguageException(String msg) {
		super(msg);
	}
	
}
