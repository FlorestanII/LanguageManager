package net.cuberia.language.exception;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2017 Johannes Pollitt
 * All rights reserved
 */

public class LanguagePoolException extends LanguageException {
	
	private static final long serialVersionUID = 4810685926071704543L;
	
	/**
	 * Creates a new {@link LanguagePoolException}
	 * 
	 * @param msg the exception message
	 */
	public LanguagePoolException(String msg) {
		super(msg);
	}
	
}
