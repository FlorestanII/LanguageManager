package net.cuberia.language.exception;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2018 Johannes Pollitt
 * All rights reserved
 */

public class LanguageException extends IllegalArgumentException{
	
	private static final long serialVersionUID = 1314590164051445560L;
	
	/**
	 * Creates a new {@link InvalidKeyException}
	 * 
	 * @param msg the exception message
	 */
	public LanguageException(String msg) {
		super(msg);
	}
	
}
